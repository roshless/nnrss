flask>=1.1.1
sqlalchemy>=1.3.12
wtforms>=2.2.1
feedparser>=5.2.1
werkzeug>=0.16.0
apscheduler>=3.6.3
listparser>=0.18
psycopg2-binary>=2.9.3
