# nnrss

### Micro rss feed reader.


Features:

- minimal web interface without javascript
- API for different clients
- multi-user capabilities
- feeds are automatically updated every N minutes, configurable for each user
- import/export (OPML)


#### Admin rights
For accessing /admin subpage (doesn't do anything yet)

```
UPDATE user SET is_admin=1 WHERE user.username=john;
```

#### Packages
Available in [AUR](https://aur.archlinux.org/packages/nnrss/)


#### Docker
Dockerfile included.

Build with:
```
docker build -t nnrss .
```

And run with (port 4321 on host):
```
docker run -p 127.0.0.1:4321:8000 -e NNRSS_DB_BACKEND=sqlite nnrss
```


#### Screenshots

[See wiki](https://man.roshless.me/~roshless/nnrss)
