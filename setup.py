import io

from setuptools import setup

with io.open("README.md", "rt", encoding="utf8") as f:
    readme = f.read()

setup(
    name='nnrss',
    author='Roshless',
    author_email='code@roshless.com',
    version='0.1',
    packages=['nnrss'],
    long_description=readme,
    include_package_data=True,
    install_requires=[
        'Flask>=1.1.1',
        'SQLAlchemy>=1.3.12',
        'WTForms>=2.2.1',
        'PyYAML>=5.2',
        'feedparser>=5.2.1',
        'Werkzeug>=0.16.0',
        'APScheduler>=3.6.3',
        'listparser>=0.18',
    ],
)
