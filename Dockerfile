FROM python:3.9-alpine

WORKDIR /app
COPY . /app

RUN apk add --no-cache py3-pip tzdata
RUN pip install -r requirements.txt gunicorn

CMD [ "gunicorn", "-b", "0.0.0.0:8000", "nnrss:APP" ]
