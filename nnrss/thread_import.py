from threading import Thread

from nnrss.database import RssHandler
from nnrss.opml import OPMLReader


class ImportThread(Thread):
    def __init__(self, data, username: str):
        Thread.__init__(self)
        self.data = data
        self.username = username

    def run(self):
        reader = OPMLReader(self.data)
        handler = RssHandler()
        handler.add_feeds(reader.get_feeds(), self.username, reader.get_categories())
