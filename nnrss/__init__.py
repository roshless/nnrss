from flask import Flask

__author__ = "Roshless <me@roshless.com>"
__version__ = "1.4.0"


def create_app():
    from nnrss.config import ACCOUNT
    from nnrss.models import SESSION

    app = Flask(__name__)
    from nnrss.api import API  # noQA isort:skip
    from nnrss.views import DEFAULT_WEB  # noQA isort:skip

    if ACCOUNT.api_enabled:
        app.register_blueprint(API, url_prefix="/api")
    if ACCOUNT.interface_enabled:
        app.register_blueprint(DEFAULT_WEB, url_prefix="/")
    if ACCOUNT.metrics_enabled:
        from prometheus_flask_exporter import NO_PREFIX
        from prometheus_flask_exporter.multiprocess import PrometheusMetrics

        metrics = PrometheusMetrics(app, defaults_prefix=NO_PREFIX)
        metrics.info("app_version", "Application version", version=__version__)

    app.secret_key = ACCOUNT.get_secret_key()

    app.config.update(
        SESSION_COOKIE_HTTPONLY=True,
        SESSION_COOKIE_SAMESITE="Lax",
    )

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        SESSION.remove()

    return app


APP = create_app()
