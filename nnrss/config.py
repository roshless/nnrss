from configparser import ConfigParser
from os import environ, path, urandom
from sys import exit, stderr


class ConfigManager:
    config_filename = "config.ini"

    section_access = "access"
    section_database = "database"

    api_enabled = False
    interface_enabled = True
    registration_enabled = False
    metrics_enabled = True
    secret_key: bytes = urandom(24)

    backend = "postgresql"
    db_username = "username"
    db_password = "password"
    db_host = "localhost"
    db_port = "5432"
    db_name = "nnrss"

    conn_string = "sqlite:///nnrss.db"

    def __load_config(self, loaded_config: ConfigParser):
        loaded_config.read(self.config_filename)

        self.api_enabled = loaded_config.getboolean(self.section_access, "api_enabled")
        self.interface_enabled = loaded_config.getboolean(
            self.section_access, "interface_enabled"
        )
        self.registration_enabled = loaded_config.getboolean(
            self.section_access, "registration_enabled"
        )
        self.metrics_enabled = loaded_config.getboolean(
            self.section_access, "metrics_enabled"
        )

        self.secret_key = loaded_config[self.section_access]["secret_key"]

        #self.broker_string = loaded_config[self.section_queue]["broker"]

        self.backend = loaded_config[self.section_database]["backend"]

    def __init__(self):
        loaded_config = ConfigParser()

        if path.exists(self.config_filename):
            self.__load_config(loaded_config)
        else:
            # Load from enviroment variables
            loaded_config = None
            if val := environ.get("NNRSS_DB_BACKEND"):
                self.backend = val
            else:
                print("NNRSS_DB_BACKEND and config not found", file=stderr)
                exit(1)

            if val := environ.get("NNRSS_SECRET_KEY"):
                self.secret_key = val
            else:
                print("NNRSS_SECRET_KEY not set, it will be random every restart")

        if self.secret_key == "change_me":
            print("Set secret key in config to something random!", file=stderr)
            exit(1)

        if self.backend == "postgresql":
            if loaded_config:
                self.db_username = loaded_config[self.section_database]["username"]
                self.db_password = loaded_config[self.section_database]["password"]
                self.db_host = loaded_config[self.section_database]["host"]
                self.db_port = loaded_config[self.section_database]["port"]
                self.db_name = loaded_config[self.section_database]["name"]
            else:
                try:
                    self.db_username = environ["NNRSS_DB_USERNAME"]
                    self.db_password = environ["NNRSS_DB_PASSWORD"]
                    self.db_host = environ["NNRSS_DB_HOST"]
                    self.db_port = environ["NNRSS_DB_PORT"]
                    self.db_name = environ["NNRSS_DB_NAME"]
                except KeyError:
                    print("Not all NNRSS_DB_* variables found!", file=stderr)
                    print(
                        "Need: NNRSS_DB_USERNAME, NNRSS_DB_PASSWORD, "
                        "NNRSS_DB_HOST, NNRSS_DB_PORT, NNRSS_DB_NAME",
                        file=stderr,
                    )
                    exit(2)

            self.conn_string = (
                f"{self.backend}://{self.db_username}:{self.db_password}"
                f"@{self.db_host}:{self.db_port}/{self.db_name}"
            )

    def is_registration_enabled(self):
        return self.registration_enabled

    def is_metrics_enabled(self):
        return self.metrics_enabled

    def get_secret_key(self):
        return self.secret_key

    def get_database_connection_string(self):
        return self.conn_string


ACCOUNT = ConfigManager()
