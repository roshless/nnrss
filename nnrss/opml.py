from datetime import datetime as dt
from typing import List, TypedDict
from xml.dom import minidom
from xml.etree.ElementTree import Element, SubElement, tostring

import listparser
from werkzeug.datastructures import FileStorage


class OPMLReader:
    """OPMLReader imports feeds from .opml file using listparser library."""

    def __init__(self, file: FileStorage):
        self.feeds_list = []
        self.categories_list = []

        parsed = listparser.parse(file.read().decode("utf-8"))
        feeds = parsed.feeds
        for feed in feeds:
            self.feeds_list.append(feed.url)
            self.categories_list.append(feed.tags[0])

    def get_feeds(self):
        """Return list of feeds"""
        return self.feeds_list

    def get_categories(self):
        """Return list of categories"""
        return self.categories_list


class OPMLWriter:
    """OPMLWriter writes and exports user's feeds"""

    title_text = "nano nano rss exported feeds"

    def __init__(self, feeds: List[TypedDict]):
        self.feeds_list = feeds

    def get_exported_feeds(self) -> str:
        root = Element("opml")
        root.set("version", "2.0")
        head = SubElement(root, "head")
        title = SubElement(head, "title")
        title.text = self.title_text
        date_created = SubElement(head, "dateCreated")
        date_created.text = str(dt.now())
        body = SubElement(root, "body")

        for feed in self.feeds_list:
            if "changed" in feed:
                category = SubElement(body, "outline", {"text": feed["category"]})
            SubElement(
                category,
                "outline",
                {
                    "type": "rss",
                    "text": feed["title"],
                    "xmlUrl": feed["rss_link"],
                    "htmlUrl": feed["link"],
                },
            )
        # Remove later and replace with indent (python 3.9)
        reparsed = minidom.parseString(tostring(root, "utf-8"))
        return reparsed.toprettyxml()
        # return tostring(root, 'utf-8')
