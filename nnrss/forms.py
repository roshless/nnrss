"""Module containing wtforms definitions"""
from wtforms import (
    FileField,
    Form,
    IntegerField,
    PasswordField,
    StringField,
    SubmitField,
    validators,
)


def username_validator():
    return [validators.Length(min=4, max=25)]


class LoginForm(Form):
    username = StringField("Username", username_validator())
    password = PasswordField("Password", [validators.DataRequired()])


class RegistrationForm(Form):
    username = StringField("Username", username_validator())
    password = PasswordField(
        "Password",
        [
            validators.DataRequired(),
            validators.EqualTo("confirm", message="Passwords must match"),
        ],
    )
    confirm = PasswordField("Repeat password")


class SettingsForm(Form):
    # needs 200 chars for hash
    password = StringField("Password", [validators.Length(min=5, max=200)])
    api_key = StringField("API key", [validators.Length(min=8, max=100)])
    update_interval = IntegerField(
        "Update interval in minutes", [validators.NumberRange(min=5, max=2880)]
    )


class AddFeedForm(Form):
    url = StringField("Feed link", [validators.URL()])
    category = StringField("Category", [validators.Length(min=1, max=25)])


class ImportForm(Form):
    import_file = FileField("File to import", [validators.DataRequired()])


class ExportForm(Form):
    export_button = SubmitField("Export feeds")
