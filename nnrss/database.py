"""Module handling database objects manipulation"""
from contextlib import contextmanager
from datetime import datetime as dt
from time import mktime
from typing import List, Optional, Tuple, TypedDict

from feedparser import parse as feedparser_parse
from sqlalchemy import exists
from sqlalchemy.orm import Query

from nnrss.models import SESSION, Category, Feed, FeedEntry, User


class RssHandler:
    @contextmanager
    def __session_scope(self):
        session = SESSION()
        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()

    def __get_user(self, input_username):
        with self.__session_scope() as session:
            user: User = session.query(User).filter_by(username=input_username)
            return user

    def __get_users(self):
        with self.__session_scope() as session:
            return session.query(User)

    def __api_key_get_username(self, username: str, api_key: str):
        with self.__session_scope() as session:
            user = session.query(User).filter_by(api_key=api_key).first()
            if user is None:
                return False
            return user.username if username == user.username else False

    def __is_admin(self, input_username: str):
        with self.__session_scope() as session:
            user: User = session.query(User).filter_by(username=input_username).first()
            if user is None:
                return False
            return user.is_admin

    def __add_user(self, user):
        with self.__session_scope() as session:
            if session.query(exists().where(User.username == user.username)).scalar():
                return False
            session.add(user)
            return True

    def __add_feed(self, feeds_list: List[Feed]):
        with self.__session_scope() as session:
            for feed in feeds_list:
                session.add(feed)

    def __add_entries(self, feed_entries_list):
        with self.__session_scope() as session:
            for feed in feed_entries_list:
                session.add(feed)

    def __get_all_feeds(self):
        with self.__session_scope() as session:
            return session.query(Feed)

    def __get_all_feeds_by_username(self, username):
        with self.__session_scope() as session:
            return session.query(Feed).join(User).filter(User.username == username)

    def __get_category_feeds_by_username(self, username, category):
        with self.__session_scope() as session:
            return (
                session.query(Feed)
                .join(User)
                .join(Category)
                .filter(User.username == username)
                .filter(Category.name == category)
            )

    def __get_feed_by_username(self, feed_id, username):
        with self.__session_scope() as session:
            return (
                session.query(Feed)
                .join(User)
                .filter(User.username == username)
                .filter(Feed.id == feed_id)
            )

    def __get_feed(self, input_feed_id: int):
        with self.__session_scope() as session:
            return session.query(Feed).filter_by(id=input_feed_id)

    def __update_feed_by_id(self, feed_id):
        with self.__session_scope() as session:
            feed = session.query(Feed).filter(Feed.id == feed_id).first()
            if not hasattr(feed, "rss_link"):
                return False
            parsed_entries = self.__parse_feed(feed.rss_link, only_entries=True)
            if parsed_entries is None:
                return False
            new_entries = [x for x in parsed_entries if x not in set(feed.entries)]
            feed.entries.extend(new_entries)
            return True

    def __update_feed_by_id_and_username(self, feed_id, username):
        with self.__session_scope() as session:
            feed = (
                session.query(Feed)
                .filter(User.username == username)
                .filter(Feed.id == feed_id)
                .first()
            )
            if not hasattr(feed, "rss_link"):
                return False
            parsed_entries = self.__parse_feed(feed.rss_link, only_entries=True)
            new_entries = [x for x in parsed_entries if x not in set(feed.entries)]
            feed.updated_date = dt.utcnow()
            feed.entries.extend(new_entries)
            return True

    def __remove_old_entries(self, feed_id):
        with self.__session_scope() as session:
            feed = session.query(Feed).filter_by(id=feed_id).first()

            for entry in feed.entries:
                if entry.is_read:
                    time_between = dt.now() - entry.date
                    if time_between > 60:
                        feed.entries.remove(entry)

    def __get_entries_by_id(self, feed_id):
        with self.__session_scope() as session:
            return (
                session.query(FeedEntry)
                .filter_by(id_author=feed_id)
                .order_by(FeedEntry.date.desc())
            )

    def __get_entries_by_id_and_username(self, feed_id, username):
        with self.__session_scope() as session:
            return (
                session.query(FeedEntry)
                .join(Feed)
                .join(User)
                .filter(FeedEntry.id_feed == feed_id)
                .filter(User.username == username)
                .order_by(FeedEntry.date.desc())
            )

    def __get_unread_entries_by_id_and_username(self, feed_id, username):
        with self.__session_scope() as session:
            return (
                session.query(FeedEntry)
                .join(Feed)
                .join(User)
                .filter(FeedEntry.id_feed == feed_id)
                .filter(FeedEntry.is_read is False)
                .filter(User.username == username)
                .order_by(FeedEntry.date.desc())
            )

    def __get_unread_entries(self, username):
        with self.__session_scope() as session:
            return (
                session.query(FeedEntry)
                .join(Feed)
                .join(User)
                .filter(FeedEntry.is_read is False)
                .filter(User.username == username)
                .order_by(FeedEntry.date.asc())
            )

    def __get_unread_entries_by_category(self, username, category):
        with self.__session_scope() as session:
            return (
                session.query(FeedEntry)
                .join(Feed)
                .join(User)
                .join(Category)
                .filter(FeedEntry.is_read is False)
                .filter(Category.name == category)
                .filter(User.username == username)
                .order_by(FeedEntry.date.asc())
            )

    def __remove_feed(self, feed_id):
        with self.__session_scope() as session:
            author = session.query(Feed).filter_by(id=feed_id).first()
            if author is None:
                return False
            session.delete(author)
            from nnrss.background_update import BACKGROUND_TASK

            BACKGROUND_TASK.start_timer()
            return True

    def __remove_feed_by_username(self, feed_id, username):
        with self.__session_scope() as session:
            author = (
                session.query(Feed)
                .join(User)
                .filter(User.username == username)
                .filter(Feed.id == feed_id)
                .first()
            )
            if author is None:
                return False
            session.delete(author)
            from nnrss.background_update import BACKGROUND_TASK

            BACKGROUND_TASK.start_timer()
            return True

    def __get_category(self, category_input: str) -> Optional[Category]:
        with self.__session_scope() as session:
            category_exists = (
                session.query(Category.id).filter_by(name=category_input).scalar()
                is not None
            )
            if category_exists:
                return session.query(Category).filter_by(name=category_input)
            session.add(Category(name=category_input))
            return session.query(Category).filter_by(name=category_input)

    def __get_all_categories(self, username: str) -> List[Category]:
        with self.__session_scope() as session:
            return (
                session.query(Category)
                .join(Feed)
                .join(User)
                .filter(User.username == username)
                .filter(Category.feeds.any())
                .order_by(Category.name)
            )

    def __add_category(self, category_input: str):
        with self.__session_scope() as session:
            session.add(Category(name=category_input))

    def __set_read_status(self, feed_id: int, entry_id: int):
        with self.__session_scope() as session:
            feed_entry = (
                session.query(FeedEntry)
                .join(Feed)
                .filter(Feed.id == feed_id)
                .filter(FeedEntry.id == entry_id)
                .first()
            )
            if feed_entry is None:
                return (False, None)
            feed_entry.is_read = True
            return (True, feed_entry.link)

    def __set_read_status_auth(self, feed_id: int, entry_id: int, username: str):
        with self.__session_scope() as session:
            feed_entry = (
                session.query(FeedEntry)
                .join(Feed)
                .join(User)
                .filter(Feed.id == feed_id)
                .filter(User.username == username)
                .filter(FeedEntry.id == entry_id)
                .first()
            )
            if feed_entry is None:
                return (False, None)
            feed_entry.is_read = True
            return (True, feed_entry.link)

    @staticmethod
    def __parse_feed(rss_url, only_entries=False):
        parsed = feedparser_parse(rss_url)
        feed_entries_list = []
        for entry in parsed.entries:
            if "description" in entry:
                content = entry.description
            else:
                content = entry.summary

            try:
                date = dt.fromtimestamp(mktime(entry.published_parsed))
            except AttributeError:
                date = dt.fromtimestamp(mktime(entry.updated_parsed))
            except ValueError:
                date = dt.fromtimestamp(1)

            feed_entries_list.append(
                FeedEntry(
                    title=entry.title,
                    link=entry.link,
                    content=content,
                    date=date,
                )
            )

        if not only_entries:
            try:
                return Feed(
                    title=parsed.feed.title,
                    link=parsed.feed.link,
                    rss_link=rss_url,
                    entries=feed_entries_list,
                )
            except AttributeError:
                return None
        else:
            return feed_entries_list

    """
    General rules:
    - If username isn't provided, there is no authorization.
    """

    def is_api_valid(self, username: str, api_key: str) -> str:
        """Check if api key is valid for user.
        Returns username if api key valid."""
        return self.__api_key_get_username(username, api_key)

    def is_user_admin(self, username: str) -> bool:
        """Checks if user is admin."""
        return self.__is_admin(username)

    def add_user(self, user: User) -> bool:
        """Returns True if user added."""
        return self.__add_user(user)

    def get_user_password(self, username: str) -> str:
        """Returns hashed password"""
        user = self.__get_user(username).first()
        return user.password if user is not None else None

    def get_user(self, username: str) -> User:
        return self.__get_user(username).first()

    def get_users(self) -> List[User]:
        users = self.__get_users().all()
        with self.__session_scope() as session:
            """lose all connection with database,
            needed for passing around username"""
            session.expunge_all()
        return users

    def get_feeds_count(self, username: str) -> int:
        """For admin panel"""
        return self.__get_all_feeds_by_username(username).count()

    def add_feeds(
        self, feeds_urls_list: List[str], feed_owner: str, category: List[str] = None
    ) -> Optional[List[Feed]]:
        """Also parses new feeds."""
        feeds_list = []
        for index, rss in enumerate(feeds_urls_list):
            feed = self.__parse_feed(rss)
            if feed is None:
                continue  # URL isn't rss feed
            feed.id_user = self.get_user(feed_owner).id
            feed.id_category = self.get_or_add_category(category[index]).id
            feeds_list.append(feed)
        self.__add_feed(feeds_list)
        return feeds_list

    def feed_exists(self, feed_id: int) -> bool:
        """Doesn't check for authorization"""
        feed = self.__get_feed(feed_id).first()
        if feed:
            return True
        return False

    def get_feeds(self, username: str = None) -> List[TypedDict]:
        """Aside from returning feeds, also sorts them by category
        and sets 'changed' attribute to indicate when new category starts"""
        feeds = self.__get_all_feeds_by_username(username)
        if feeds.count() == 0:
            return []
        output_feed_list = [
            {
                "id": f.id,
                "title": f.title,
                "link": f.link,
                "rss_link": f.rss_link,
                "category": f.category.name,
            }
            for f in feeds
        ]
        output_feed_list.sort(key=lambda k: k["category"])
        last_seen_category = ""
        for feed in output_feed_list:
            if feed["category"] != last_seen_category:
                feed["changed"] = True
            last_seen_category = feed["category"]
        return output_feed_list

    def get_feeds_from_category(
        self, username: str = None, category: str = None
    ) -> List[TypedDict]:
        if not category:
            raise Exception
        feeds = self.__get_category_feeds_by_username(username, category)
        if feeds.count() == 0:
            return []
        return [
            {
                "id": f.id,
                "title": f.title,
                "link": f.link,
                "rss_link": f.rss_link,
            }
            for f in feeds
        ]

    def get_feed_without_details(self, feed_id: int, username: str):
        """Returns only one feed's id, title and url, used in web view"""
        the_feed = self.__get_feed_by_username(feed_id, username).first()
        return dict(
            id=the_feed.id,
            title=the_feed.title,
            link=the_feed.link,
            category=the_feed.category.name,
        )

    def get_entries_by_feed_id(self, feed_id: int, username: str = None) -> Query:
        if username is None:
            return self.__get_entries_by_id(feed_id)
        return self.__get_entries_by_id_and_username(feed_id, username)

    def get_unread_entries_by_feed_id(
        self, feed_id: int, username: str = None
    ) -> Query:
        if username is None:
            raise NotImplementedError
        return self.__get_unread_entries_by_id_and_username(feed_id, username)

    def get_unread_entries(self, username: str = None, category: str = None) -> Query:
        if not username:
            raise NotImplementedError
        if category:
            return self.__get_unread_entries_by_category(username, category)
        else:
            return self.__get_unread_entries(username)

    def update_feed(self, feed_id: int, username: str = None) -> bool:
        """Get new entries for feed."""
        if username is None:
            return self.__update_feed_by_id(feed_id)
        return self.__update_feed_by_id_and_username(feed_id, username)
        # TODO removing old entries
        # self.__remove_old_entries(feed_id)

    def remove_feed(self, feed_id: int, username: str = None) -> bool:
        if username is None:
            return self.__remove_feed(feed_id)
        return self.__remove_feed_by_username(feed_id, username)

    def get_or_add_category(self, category: str) -> Category:
        return self.__get_category(category).first()

    def get_all_categories(self, username: str) -> List[Category]:
        return self.__get_all_categories(username).all()

    def set_read_entry(
        self, feed_id: int, entry_id: int, username: str
    ) -> Tuple[bool, Optional[str]]:
        """Won't work with wrong coresponding ids."""
        if username is None:
            return self.__set_read_status(feed_id, entry_id)
        return self.__set_read_status_auth(feed_id, entry_id, username)
