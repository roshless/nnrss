import datetime
import os

from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    create_engine,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, scoped_session, sessionmaker

from nnrss.config import ACCOUNT
from nnrss.pagination import PaginateQuery

ENGINE = create_engine(ACCOUNT.get_database_connection_string())
BASE = declarative_base()
SESSION = scoped_session(sessionmaker(bind=ENGINE, query_cls=PaginateQuery))


class User(BASE):
    __tablename__ = "user"

    def __init__(self, username: str, password: str):
        self.username = username
        self.password = password

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    password = Column(String)
    feeds = relationship("Feed", back_populates="user")
    is_admin = Column(Boolean, default=False)
    api_key = Column(String, default=os.urandom(16).hex())
    update_interval = Column(Integer, default=30)


class Category(BASE):
    __tablename__ = "category"

    id = Column(Integer, primary_key=True)
    name = Column(String)


class Feed(BASE):
    __tablename__ = "feed"

    id = Column(Integer, primary_key=True)
    id_user = Column(Integer, ForeignKey("user.id"))
    user = relationship(User, back_populates="feeds")
    id_category = Column(Integer, ForeignKey("category.id"))
    category = relationship(Category, backref="feeds")
    title = Column(String)
    link = Column(String)
    rss_link = Column(String)
    entries = relationship(
        "FeedEntry", back_populates="feed", cascade="all, delete-orphan"
    )
    updated_date = Column(DateTime, default=datetime.datetime.utcnow)


class FeedEntry(BASE):
    __tablename__ = "feed_entry"

    id = Column(Integer, primary_key=True)
    title = Column(String)
    link = Column(String)
    content = Column(String)
    is_read = Column(Boolean, default=False)
    id_feed = Column(Integer, ForeignKey("feed.id"))
    feed = relationship(Feed, back_populates="entries")
    date = Column(DateTime, default=datetime.datetime.utcnow)

    def __hash__(self):
        return hash((self.title, self.link, self.date))

    def __eq__(self, other):
        try:
            return (self.title, self.link, self.date) == (
                other.title,
                other.link,
                other.date,
            )
        except AttributeError:
            return NotImplemented


BASE.metadata.create_all(ENGINE)
